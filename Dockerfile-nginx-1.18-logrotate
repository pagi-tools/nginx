FROM nginx:1.18.0

MAINTAINER jeremyli

# Set the reset cache variable
ENV REFRESHED_AT 2016-05-10

# Create folder
RUN mkdir -p /src

# Update system and install required software
RUN apt-get update &&\
    apt-get install -y wget \
                       curl \
                       git \
                       build-essential \
                       autoconf \
                       libtool \
                       libpcre3 \
                       libpcre3-dev \
                       libssl-dev \
                       zlib1g-dev \
                       libxslt-dev \
                       gcc \
                       libgd-dev \
                       libc6-dev \
                       libgeoip-dev \
                       logrotate

# Download MaxMind GeoLite2 databases
RUN mkdir -p /usr/share/GeoIP/
COPY ./src/GeoLite2-Country.mmdb /usr/share/GeoIP/

# Install C library for reading MaxMind DB files
COPY ./src/libmaxminddb.tar.gz /
RUN tar -zxvf libmaxminddb.tar.gz

RUN cd libmaxminddb &&\
    ./bootstrap &&\
    ./configure &&\
    make &&\
    make check &&\
    make install &&\
    echo /usr/local/lib  >> /etc/ld.so.conf.d/local.conf &&\
    ldconfig

# Download Nginx and the Nginx geoip2 module
COPY ./src/nginx-1.18.0.tar.gz / 
COPY ./src/ngx_http_geoip2_module.tar.gz /
COPY ./src/nginx-module-vts.tar.gz /
COPY ./src/nginx-module-sts.tar.gz /
COPY ./src/nginx-module-stream-sts.tar.gz /
COPY ./src/LuaJIT-2.0.5.tar.gz /
COPY ./src/lua-nginx-module-0.10.19.tar.gz /
COPY ./src/ngx_devel_kit-0.3.1.tar.gz /
COPY ./src/openresty-1.19.3.1.tar.gz /
COPY ./src/ngx_brotli.tar.gz /
COPY ./src/libbrotli.tar.gz /

RUN tar -zxvf nginx-1.18.0.tar.gz
RUN tar -zxvf ngx_http_geoip2_module.tar.gz
RUN tar -zxvf nginx-module-vts.tar.gz 
RUN tar -zxvf nginx-module-sts.tar.gz
RUN tar -zxvf nginx-module-stream-sts.tar.gz
RUN tar -zxvf LuaJIT-2.0.5.tar.gz
RUN tar -zxvf lua-nginx-module-0.10.19.tar.gz
RUN tar -zxvf ngx_devel_kit-0.3.1.tar.gz
RUN tar -zxvf openresty-1.19.3.1.tar.gz
RUN tar -zxvf ngx_brotli.tar.gz
RUN tar -zxvf libbrotli.tar.gz

RUN cd /libbrotli &&\
    ./autogen.sh &&\
    ./configure &&\
    make &&\
    make install

RUN cd /ngx_brotli &&\ 
    git submodule update --init 

RUN cd /LuaJIT-2.0.5 \
    && make -j`nproc` \
    && make install \
    && ldconfig \
    && export LUAJIT_LIB=/usr/local/lib \
    && export LUAJIT_INC=`cd /usr/local/include/lua* && pwd`

RUN cp /usr/local/lib/libluajit-5.1.so.2 /usr/lib/


WORKDIR /nginx-1.18.0

RUN ./configure --with-compat --add-dynamic-module=/ngx_brotli &&\
    make modules &&\
    cp ./objs/ngx_http_brotli*.so /etc/nginx/modules

# Compile Nginx
RUN export LUAJIT_LIB=/usr/local/lib &&\
    export LUAJIT_INC=`cd /usr/local/include/lua* && pwd` &&\
    ./configure --prefix=/etc/nginx \
    --sbin-path=/usr/sbin/nginx \
    --modules-path=/usr/lib64/nginx/modules \
    --conf-path=/etc/nginx/nginx.conf \
    --error-log-path=/var/log/nginx/error.log \
    --http-log-path=/var/log/nginx/access.log \
    --pid-path=/var/run/nginx.pid \
    --lock-path=/var/run/nginx.lock \
    --http-client-body-temp-path=/var/cache/nginx/client_temp \
    --http-proxy-temp-path=/var/cache/nginx/proxy_temp \
    --http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp \
    --http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
    --http-scgi-temp-path=/var/cache/nginx/scgi_temp \
    --user=nginx \
    --group=nginx \
    --with-compat \
    --with-file-aio \
    --with-threads \
    --with-http_addition_module \
    --with-http_auth_request_module \
    --with-http_dav_module \
    --with-http_flv_module \
    --with-http_gunzip_module \
    --with-http_gzip_static_module \
    --with-http_mp4_module \
    --with-http_random_index_module \
    --with-http_realip_module \
    --with-http_secure_link_module \
    --with-http_slice_module \
    --with-http_ssl_module \
    --with-http_stub_status_module \
    --with-http_sub_module \
    --with-http_v2_module \
    --with-http_xslt_module \
    --with-http_image_filter_module \
    --with-mail \
    --with-mail_ssl_module \
    --with-stream \
    --with-stream_realip_module \
    --with-stream_ssl_module \
    --with-stream_ssl_preread_module \
    --with-cc-opt='-O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector-strong --param=ssp-buffer-size=4 -grecord-gcc-switches -m64 -mtune=generic -fPIC' \
    --with-ld-opt='-Wl,-z,relro -Wl,-z,now -pie' \
    --add-module=/ngx_http_geoip2_module \
    --add-module=/nginx-module-vts \
    --add-module=/nginx-module-sts \
    --add-module=/nginx-module-stream-sts \
    --add-module=/ngx_brotli \
    --add-module=/ngx_devel_kit-0.3.1 \
    --add-dynamic-module=/lua-nginx-module-0.10.19 &&\
    make &&\
    make install
